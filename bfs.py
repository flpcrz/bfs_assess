import string

def bfs(adjacentMatrix, start): 
    # Recebendo o input start como LETRA   
    LETTERS = {letter: str(index) for index, letter in enumerate(string.ascii_uppercase, start=1)}
    start = int(LETTERS[start])   

    queue = [[start]]
    visited = set()

    graph = {}
    for i, node in enumerate(adjacentMatrix):
        adj = []
        for j, connected in enumerate(node):
            if connected:
                adj.append(j+1)
        graph[i+1] = adj

    while queue:        
        path = queue.pop(0)        
        vertex = path[-1]
        ordem_completa = []  

        # Verifica se o nó já foi visitado
        if vertex not in visited:
            # Enumera os nós adjacentes
            for current_neighbour in graph.get(vertex, []):
                new_path = list(path)
                new_path.append(current_neighbour)
                queue.append(new_path)

                if len(new_path) > 0:
                    ordem_completa.append(vertexLetterGenerator(new_path))
            # Marca como visitado
            visited.add(vertex)
            print(ordem_completa)    

def vertexLetterGenerator(elements):
    path= []
    for number in elements:
        d = dict(enumerate(string.ascii_uppercase, 1))
        path.append(d[number])
    return path

if __name__ == '__main__':
    matriz_adjacente = [[0,1,1,1,1,0,0,0], 
                        [0,0,1,1,0,0,0,0], 
                        [0,0,0,0,0,1,0,0], 
                        [0,0,1,0,1,1,1,0], 
                        [0,0,0,0,0,1,0,0],
                        [0,0,0,0,0,0,1,0],
                        [0,0,1,0,0,0,0,1],
                        [0,0,1,0,0,0,0,0]]

    bfs(matriz_adjacente, 'A')
